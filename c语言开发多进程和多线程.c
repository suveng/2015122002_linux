/**
 * c语言开发多进程和多线程
 * @author VengSu
 * 在程序中获取环境变量
 */
#include<stdio.h>
	#include<stdlib.h>
	int main(){
	char *env_path = "PATH";
	char *env_value= NULL;
	
	env_value = getenv(env_path);
	if(NULL==env_value){
		printf("not found!\n");	
	}
	else{
		printf("get env_path:\n%s",env_value);
	}
	return 0;
}