/*
 * 在父进程中使用waitpid()函数等待制定进程号的子进程返回
 */
#include<sys/types.h>
	#include <unistd.h>
	#include<stdio.h>
	#include<stdlib.h>
	#include<sys/wait.h>
	int main(){
	pid_t pid, pid_wait;
	int status;
	pid =fork();
	if(-1==pid){
		printf("error to create new process!\n");
		return 0;
	}
	else if( pid ==0){
		printf("chlid process !\n");
		
	}else {
		printf("parent process ! child process id: %d\n",pid);
		pid_wait=waitpid(pid,&status,0);
		printf("child process %d returned!\n",pid_wait);
	}
	return 0;
}
