/*用无名管道进行通信
 *采取剧本的方式
 *2017-3-6
 * */
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<sys/wait.h>
#include<time.h>

#define MAX_MSG_SIZE 256

int main()
{
    char* parent_talk[] = {"hello", "what is the time?", "ok, bye", NULL};
    char* child_talk[] = {"hi", "time is ", "bye", NULL};

    int fd1[2];
    int fd2[2];
    int res = pipe(fd1);
    if(res == -1)
    {
        perror("pipe");
        exit(EXIT_FAILURE);
    }
    res = pipe(fd2);
    if(res == -1)
    {
        perror("pipe");
        exit(EXIT_FAILURE);
    }
    pid_t pid = fork();
    if(pid == 0)//child
    {
        close(fd1[1]);
        close(fd2[0]);

        char buf[MAX_MSG_SIZE];
        int i = 0;
        char *talk = child_talk[i];
        while(talk != NULL)
        {
            read(fd1[0], buf, MAX_MSG_SIZE);
            printf("parent say:>%s\n", buf);
            write(fd2[1], talk, strlen(talk)+1);
            i++;
            talk = child_talk[i];
        }
        close(fd1[0]);
        close(fd2[1]);
    }
    else if(pid > 0)//parent
    {
        close(fd1[0]);
        close(fd2[1]);

        char buf[MAX_MSG_SIZE];
        int i = 0;
        char *talk = parent_talk[i];
        while(talk != NULL)
        {
            write(fd1[1], talk, strlen(talk)+1);
            read(fd2[0], buf, MAX_MSG_SIZE);
            if(strcmp(buf, child_talk[1]) == 0)
            {
                time_t timep;
                struct tm *p;
                time(&timep);;
                strcat(buf, ctime(&timep));
            }
            printf("child :>%s\n", buf);
            i++;
            talk = parent_talk[i];
        }
        close(fd1[1]);
        close(fd2[0]);
        int stat;
        wait(&stat);
    }
    else
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    return 0;
}