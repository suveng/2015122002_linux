#include <stdio.h>
#include <stdlib.h>

static int get_char(){
	fd_set rfds;
	struct timeval tv;
	int ch = -1;

	FD_ZERO(&rfds);
	FD_SET(0,&rfds);
	tv.tv_sec=0;
	tv.tv_usec =10;
	if(select(1,&rfds,NULL,NULL,&tv)>0){
		ch=getchar();	
	}
	return ch;
}

int main(int argc,char argv){
	int ch =0;
 	system("stty raw -echo -F /dev/tty");
	ch =get_char();
	while(ch!='\n'&&ch!='\r')
	{
		if(ch>0){
			printf("输入了(%c)\n",ch);
		}
		else{
			usleep(10);
		}
		ch =get_char();
	}
	system("stty -raw echo -F /dev/tty");
	return 0;
}